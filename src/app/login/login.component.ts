import { Component, OnInit } from '@angular/core';
import {AuthRestService} from '../rest/auth.rest.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthRestService]
})
export class LoginComponent implements OnInit {
  public loginData = {username: '', password: ''};

  constructor(private service: AuthRestService) {}

  login() {
    this.service.obtainAccessToken(this.loginData);
  }

  ngOnInit(): void {
  }

}
