import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {UserRestService} from '../rest/user.rest.service';
import {UserDTO} from '../dto/user.dto.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserRestService]
})
export class RegisterComponent implements OnInit {
  genders = ['male', 'female'];
  signupForm: FormGroup;
  // Minimum eight characters, at least one letter and one number:
  regexp = new RegExp('^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,}$');

  constructor(private rest: UserRestService, private router: Router) {
  }

  ngOnInit(): void {

    this.signupForm = new FormGroup({
      'username': new FormControl(null, [Validators.required], this.duplicatedNames.bind(this)),
      'email': new FormControl(null, [Validators.required, Validators.email], this.duplicatedEmails.bind(this)),
      'password': new FormControl(null, [Validators.required], this.password.bind(this)),
      'passwordMatch': new FormControl(null, [Validators.required], this.passMatch.bind(this)),
      'gender': new FormControl('male')
    });
  }

  duplicatedNames(control: FormControl) {
   return this.rest.checkUsername(control.value);
  }

  duplicatedEmails(control: FormControl): Observable< {[s: string]: boolean }> {
    return this.rest.isValidEmail(control.value);
  }

  password(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      if (!this.regexp.test(control.value)) {
        resolve({'incorrectPassword': true});
      } else {
        resolve(null);
      }
    });
    return promise;
  }

  passMatch(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      if (!(this.signupForm.get('password').value === control.value)) {
        resolve({'passwordsNotMatch': true});
      } else {
        resolve(null);
      }
    });
    return promise;

  }

  onSubmit() {

    if (!this.signupForm.valid) {
      return;
    }

    const userDto: UserDTO = new UserDTO();
    userDto.username = this.signupForm.get('username').value;
    userDto.password = this.signupForm.get('password').value;
    userDto.email = this.signupForm.get('email').value;

    const message = this.rest.register(userDto);
    this.signupForm.reset();

    if (message === 'OK') {
      this.router.navigate(['/after-registration']);
    }
  }
}

