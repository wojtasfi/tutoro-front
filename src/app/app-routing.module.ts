import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorPageComponent} from './info-pages/error-page/error-page.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './register/register.component';
import {AfterRegisterPageComponent} from './info-pages/after-register-page/after-register-page.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found'}},
  {path: 'after-registration', component: AfterRegisterPageComponent},
  {path: '**', redirectTo: '/not-found'}
// ** means catch all you do not know- must be the last route
//  { path: '', redirectTo: '/somewhere-else', pathMatch: 'full' }
];

@NgModule({
  imports: [
    // webserver will ignore things after hashtag
    // RouterModule.forRoot(appRoutes, {useHash: true})],
    RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
