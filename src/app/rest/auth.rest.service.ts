import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Cookie} from 'ng2-cookies';
import {HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {TutorDetails} from '../tutor-details.model';
import {RestService} from './rest.service';


@Injectable()
export class AuthRestService extends RestService {


  obtainAccessToken(loginData) {
    const params = new URLSearchParams();
    params.append('username', loginData.username);
    params.append('password', loginData.password);
    params.append('grant_type', 'password');
    params.append('scope', 'webclient');
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa('tutoro_front:thisissecret')
    });

    this.http.post('http://localhost:5555/api/auth/oauth/token',
      params.toString(),
      {headers: headers})
      .subscribe(
        data => this.saveToken(data),
        err => alert('Invalid Credentials'));
  }

  saveToken(token) {
    const expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set('access_token', token.access_token, expireDate);
    this.router.navigate(['/home']);
  }

  getResource(resourceUrl): Observable<TutorDetails> {
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + Cookie.get('access_token')
    });
    return this.http.get(resourceUrl, {headers: headers})
      .pipe(
        catchError((error: any) => Observable.throw(error.json().error || 'Server error'))
      );
  }

  checkCredentials() {
    if (!Cookie.check('access_token')) {
      this.router.navigate(['/login']);
    }
  }

  logout() {
    Cookie.delete('access_token');
    this.router.navigate(['/login']);
  }
}
