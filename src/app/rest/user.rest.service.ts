import {HttpHeaders, HttpParams} from '@angular/common/http';
import {UserDTO} from '../dto/user.dto.model';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {RestService} from './rest.service';

@Injectable()
export class UserRestService extends RestService {

  public register(userDto: UserDTO): string {

    const message = 'OK';
    const headers = new HttpHeaders({
      'Content-type': 'application/json'
    });

    const registerUrl = this.getBaseUrl() + '/users/register';
    this.http.post<UserDTO>(registerUrl, userDto, {headers: headers})
      .subscribe(
        data => console.log(data),
        err => this.handleError(err));

    return message;
  }

  // validateEmail(token: string) {
  //   let message = 'OK';
  //
  //   const registerUrl = this.url + '/validate/email/';
  //   this.http.post<UserDTO>(registerUrl, token)
  //     .subscribe(
  //       data => console.log(data),
  //       err => message = 'ERROR');
  //
  //   return message;
  //
  // }

  public isValidEmail(email: string): Observable<any> {
    const params = new HttpParams().set('email', email);
    return this.http.get<boolean>(this.getBaseUrl() + '/validate/email', {params: params});

  }

  public checkUsername(username: string): Observable<any> {
    const params = new HttpParams().set('username', username);
    return this.http.get<boolean>(this.getBaseUrl() + '/validate/username', {params: params});
  }


}
