import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserRestService} from '../../rest/user.rest.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-after-register-page',
  templateUrl: './after-register-page.component.html',
  styleUrls: ['./after-register-page.component.css'],
  providers: [UserRestService]
})
export class AfterRegisterPageComponent implements OnInit {

  emailValidForm: FormGroup;
  constructor(private rest: UserRestService, private router: Router) { }

  ngOnInit() {
    this.emailValidForm = new FormGroup({
      'token': new FormControl(null, [Validators.required])
    });
  }

  // onSubmit() {
  //   this.rest.validateEmail(this.emailValidForm.get('token').value);
  //
  // }

}
