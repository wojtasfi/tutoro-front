import { Component, OnInit } from '@angular/core';
import {AuthRestService} from '../rest/auth.rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AuthRestService]
})
export class HomeComponent implements OnInit {

  constructor(
    private service: AuthRestService) {}

  ngOnInit() {
    this.service.checkCredentials();
  }

  logout() {
    this.service.logout();
  }

}
